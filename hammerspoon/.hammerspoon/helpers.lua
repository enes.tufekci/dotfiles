local helpers = {}

function helpers.dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function helpers.len(o)
  local i = 0
  if type(o) == 'table' then
    for k,v in pairs(o) do
      i = i + 1
    end
  end
  return i
end

local clock = os.clock
function helpers.sleep(n)  -- seconds
  local t0 = clock()
  while clock() - t0 <= n do end
end

return helpers
