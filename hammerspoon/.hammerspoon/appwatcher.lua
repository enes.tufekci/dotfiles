onActivated = nil
onLaunched = nil
local appwatcher = {}

function applicationWatcher(appName, eventType, appObject)
  if (eventType == hs.application.watcher.activated) then
    onActivated(appName, appObject)
  end
  if (eventType == hs.application.watcher.launched) then
    onLaunched(appName, appObject)
  end

end

function appwatcher.onActivated(fn)
  onActivated = fn
end

function appwatcher.onLaunched(fn)
  onLaunched = fn
end

function appwatcher.start()
  appWatcher = hs.application.watcher.new(applicationWatcher)
  appWatcher:start()
end

return appwatcher
