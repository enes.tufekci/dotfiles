-- Imports
screen = require("screen")
helpers = require("helpers")
appwatcher = require("appwatcher")

-- Config
hs.window.animationDuration = 0
-- Constants
SPLIT_RATIO_TABLE = {{1,2}, {2,3}, {1,1}, {3,2}, {2,1}}

-- State
STATE = {}
STATE['SplitCmdIssued'] = false
STATE['SplitApps'] = {}
STATE['SplitRatio'] = 3
STATE['MODE'] = 'W'

-- Config
CONFIG = {}

ExcludeOnEmacsFocus = nil


mode = hs.menubar.new()
mode:setTitle(STATE['MODE']);

function toggleMode()
  print(STATE['MODE'])
    if STATE['MODE'] == 'W' then
        STATE['MODE'] = 'P'
        mode:setTitle(STATE['MODE']);
    else
        STATE['MODE'] = 'W'
        mode:setTitle(STATE['MODE']);
    end
end

function hideOtherWindows(appName)
  local activeWin = hs.window.allWindows()

  for k,v in pairs(activeWin) do
    local isHammerSpoon = v:application():name() == 'Hammerspoon'
    local isFocusedWin = v:application():name() == appName
    if (isFocusedWin ~= true and isHammerSpoon ~= true) then
        v:application():hide()
    end
  end
end

function getOtherSplitAppWindow(appName)
  if(STATE['SplitApps'][0] == nil or STATE['SplitApps'][1] == nil) then
    return ''
  end
  if(STATE['SplitApps'][0]:application():name() == appName) then
    return STATE['SplitApps'][1]:application():name()
  end
  if(STATE['SplitApps'][1]:application():name() == appName) then
    return STATE['SplitApps'][0]:application():name()
  end
  return ''
end

function isAppSplit(appName)
  if(STATE['SplitApps'][0] == nil or STATE['SplitApps'][1] == nil) then
    return false
  end
  if(STATE['SplitApps'][0]:application():name() == appName) then
    return true
  end
  if(STATE['SplitApps'][1]:application():name() == appName) then
    return true
  end
  return false
end

-- Helpers
function emacsFocus(appName, appObject)
  if(appName == "Emacs") then
    local activeWin = hs.window.allWindows()
    local isEmacsInSplitApps = isAppSplit('Emacs')
    local otherSplitApp = getOtherSplitAppWindow('Emacs')

    for k,v in pairs(activeWin) do
      local isEmacs = v:application():name() == 'Emacs'
      local isHammerSpoon = v:application():name() == 'Hammerspoon'
      local isOtherSplitApp = otherSplitApp ~= '' and v:application():name() == otherSplitApp
      local isExcludedApp = v:application():name() == ExcludeOnEmacsFocus
      if (isEmacs ~= true and isHammerSpoon ~= true and isOtherSplitApp ~= true and isExcludedApp ~= true) then
          v:application():hide()
      end
    end
  end
  if (appName == "Finder") then
      -- Bring all Finder windows forward when one gets activated
      appObject:selectMenuItem({"Window", "Bring All to Front"})
  end
end


function resizeSplit(win1, win2, ratio)
  screen.split(win1, win2, ratio)
end

function shiftFocusToLeft()
  if STATE['SplitApps'][1] ~= nil then
    hs.application.launchOrFocus(STATE['SplitApps'][1]:application():name())
  end
end

function shiftFocusToRight()
  if STATE['SplitApps'][0] ~= nil then
    hs.application.launchOrFocus(STATE['SplitApps'][0]:application():name())
  end
end

function shiftSplitRatioToRight()
  if STATE['SplitRatio'] <= 5 then
    STATE['SplitRatio'] = STATE['SplitRatio'] + 1
  end
  screen.split(STATE['SplitApps'][1], STATE['SplitApps'][0], SPLIT_RATIO_TABLE[STATE['SplitRatio']])
end

function shiftSplitRatioToLeft()
  if STATE['SplitRatio'] >= 1 then
    STATE['SplitRatio'] = STATE['SplitRatio'] - 1
  end
  screen.split(STATE['SplitApps'][1], STATE['SplitApps'][0], SPLIT_RATIO_TABLE[STATE['SplitRatio']])
end

function split()
  STATE['SplitApps'] = {}
  STATE['SplitRatio'] = 3

  local win1 = hs.window.focusedWindow()
  hideOtherWindows(win1:application():name())
  STATE['SplitCmdIssued'] = true
  STATE['SplitApps'][0] = win1
  hs.application.launchOrFocus('Launchpad')
end

function handleAfterSplit()
  if STATE['SplitCmdIssued'] == true then
    STATE['SplitCmdIssued'] = false
    local win2 = hs.window.focusedWindow();
    STATE['SplitApps'][1] = win2
    screen.split(STATE['SplitApps'][1], STATE['SplitApps'][0], SPLIT_RATIO_TABLE[STATE['SplitRatio']])
  end
end

function emacs()
  local win = hs.window.focusedWindow()
  if win:application():name() == 'Emacs' and STATE['MODE'] == 'W' then
    hs.application.launchOrFocus('Google Chrome')
  else
    hs.application.launchOrFocus('Emacs')
  end
end

function emacsAndAlacritty()
    STATE['SplitApps'] = {}

    ExcludeOnEmacsFocus = 'Alacritty'
    hs.application.launchOrFocus('Emacs')
    emacsWin = hs.window.focusedWindow()
    STATE['SplitApps'][0] = emacsWin

    hideOtherWindows('Emacs')
    hs.application.launchOrFocus('Alacritty')
    alacrittyWin = hs.window.focusedWindow()

    STATE['SplitApps'][1] = alacrittyWin

    screen.split(alacrittyWin, emacsWin, {1,2})
end

function fullScreenFocusedWindow()
  STATE['SplitApps'] = {}
  STATE['SplitRatio'] = 3
  local win = hs.window.focusedWindow()
  if win:application():name() == 'Emacs' then
    hideOtherWindows('Emacs')
  end

  screen.fullScreen(win)
end

-- Hotkeys

hotkeys = {
  {{"rightalt"}, "m", "Mail"},
  {{"rightalt"}, "w", "Brave Browser"},
  {{"rightalt"}, "k", "Slack"},
  {{"rightalt"}, "d", "Finder"},
  {{"rightalt"}, "c", "Calendar"},
  {{"rightalt"}, "p", "Zeplin"},
  {{"rightalt"}, "t", "Google Meet"},
  {{"rightalt"}, "Return", "Alacritty"},
  {{"rightalt"}, "e", emacs},
  {{"rightalt"}, "1", emacsAndAlacritty},
  {{"rightalt"}, "s", split},
  {{"rightalt"}, "f", fullScreenFocusedWindow},
  {{"rightalt", "shift"}, "]", shiftSplitRatioToRight},
  {{"rightalt", "shift"}, "[", shiftSplitRatioToLeft},
  {{"rightalt"}, "[", shiftFocusToLeft},
  {{"rightalt"}, "]", shiftFocusToRight},
  {{"rightalt"}, "0", toggleMode},
}

for k,v in pairs(hotkeys) do
  local modifier = v[1]
  local key = v[2]
  local appName = v[3]

  hs.hotkey.bind(modifier, key, function()
    if type(appName) == 'string' then
      hs.application.launchOrFocus(appName)
    else
      appName()
    end
  end)
end

-- App Watcher
appwatcher.start()
appwatcher.onActivated(function(appName, appObject)
    emacsFocus(appName, appObject)
    handleAfterSplit()
end)

appwatcher.onLaunched(function()
 fullScreenFocusedWindow()
end)
-- Config Reload

function reloadConfig(files)
    doReload = false
    for _,file in pairs(files) do
        if file:sub(-4) == ".lua" then
            doReload = true
        end
    end
    if doReload then
        hs.reload()
    end
end

myWatcher = hs.pathwatcher.new(os.getenv("HOME") .. "/dotfiles/hammerspoon/.hammerspoon/", reloadConfig):start()
hs.alert.show("Config loaded")

