STATE = {}
STATE['workspaces'] = {}
STATE['currentWorkspace'] = 1

GAP_OUTER = 16
GAP_INNER = GAP_OUTER / 2

function hide()
  local win =hs.window.focusedWindow()
  win:application():hide()
end

function fullScreen(win)
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x + GAP_OUTER
  f.y = max.y + GAP_OUTER
  f.w = max.w - GAP_OUTER * 2
  f.h = max.h - GAP_OUTER * 2
  win:setFrame(f)
end


hs.hotkey.bind({"rightalt"}, "f", function()

  local win =hs.window.focusedWindow()
    fullScreen(win)
end)

hs.hotkey.bind({"rightalt"}, "h", function()
    hide()
end)

hs.hotkey.bind({"rightalt"}, "t", function()
    W = {}
    W['windows'] = {}
    table.insert(W['windows'], CURRENT_WORKSPACE, WORKSPACES)
    print('Hello', dump(W))
end)

function addAppToTheWorkspace(c, appName, appObject)
  if STATE['workspaces'][c] == nil then
    STATE['workspaces'][c] = {}
  end

  if STATE['workspaces'][c]['windows'] == nil then
    STATE['workspaces'][c]['windows'] = {}
  end

  local idx = len(STATE['workspaces'][c]['windows'])
  table.insert(STATE['workspaces'][c]['windows'] , idx + 1, {appName, appObject})
  print(dump(STATE))
end

function removeAppFromTheWorkspace(c, appName)
  if STATE['workspaces'][c] == nil then
    STATE['workspaces'][c] = {}
    STATE['workspaces'][c]['windows'] = {}
  end
  local currentW = STATE['workspaces'][c]['windows']
  local out = {}
  for k,v in pairs(currentW) do
     if v[1] ~= appName then
       out[k] = v
     end
  end
  STATE['workspaces'][c]['windows'] = out
end

function onApplicationLaunched(appName, appObject)
  sleep(0.1)
  local c = STATE['currentWorkspace']
  print(appName, appObject)
  addAppToTheWorkspace(c, appName, appObject)
end
function onApplicationTerminated(appName, appObject)
  sleep(0.1)
  local c = STATE['currentWorkspace']
  removeAppFromTheWorkspace(c, appName)
end

-- Menu Item
workspaceMenuItem = hs.menubar.new()

if workspaceMenuItem then
  workspaceMenuItem:setTitle("[ " .. STATE['currentWorkspace'] .. " ]")
end

function getWorkspaceWindows(c)
  if STATE['workspaces'][c] == nil then
    STATE['workspaces'][c] = {}
    STATE['workspaces'][c]['windows'] = {}
  end
  return STATE['workspaces'][c]['windows']
end

function hideAllWorkspaceWindows(c)
    for k,v in pairs(getWorkspaceWindows(c)) do
      v[2]:hide()
    end
end

function activateWorkspace(c)
  local show = false
  for i=0,10 do
    local cand =getWorkspaceWindows(c)[i]
    if cand ~= nil then
      if show ~= true then
        cand[2]:unhide()
        sleep(0.1)
        cand[2]:mainWindow():focus()
        show = true
      end
    end
  end
end

function switchWorkspace(number)
  return function()
    local prevW = STATE['currentWorkspace']
    if number ~= prevW then
      STATE['currentWorkspace'] = number
      workspaceMenuItem:setTitle("[ " ..  STATE['currentWorkspace'] .. " ]")
      hideAllWorkspaceWindows(prevW)
      activateWorkspace(number)
    end
  print(dump(STATE))
  end
end

function moveToWorkspace(number)
  return function()
    local c =STATE['currentWorkspace']
    local win = hs.window.frontmostWindow()
    local appName = win:application():name()
    removeAppFromTheWorkspace(c, appName)
    addAppToTheWorkspace(number, appName, win:application())
    if number ~= c then
      win:application():hide()
    end
  print(dump(STATE))
  end
end

hs.hotkey.bind({"rightalt"}, "1", switchWorkspace(1))
hs.hotkey.bind({"rightalt"}, "2", switchWorkspace(2))
hs.hotkey.bind({"rightalt"}, "3", switchWorkspace(3))
hs.hotkey.bind({"rightalt"}, "4", switchWorkspace(4))
hs.hotkey.bind({"rightalt"}, "5", switchWorkspace(5))
hs.hotkey.bind({"rightalt"}, "6", switchWorkspace(6))
hs.hotkey.bind({"rightalt"}, "7", switchWorkspace(7))
hs.hotkey.bind({"rightalt"}, "8", switchWorkspace(8))
hs.hotkey.bind({"rightalt"}, "9", switchWorkspace(9))
hs.hotkey.bind({"rightalt"}, "0", switchWorkspace(0))


hs.hotkey.bind({"rightalt", "shift"}, "1", moveToWorkspace(1))
hs.hotkey.bind({"rightalt", "shift"}, "2", moveToWorkspace(2))
hs.hotkey.bind({"rightalt", "shift"}, "3", moveToWorkspace(3))
hs.hotkey.bind({"rightalt", "shift"}, "4", moveToWorkspace(4))
hs.hotkey.bind({"rightalt", "shift"}, "5", moveToWorkspace(5))
hs.hotkey.bind({"rightalt", "shift"}, "6", moveToWorkspace(6))
hs.hotkey.bind({"rightalt", "shift"}, "7", moveToWorkspace(7))
hs.hotkey.bind({"rightalt", "shift"}, "8", moveToWorkspace(8))
hs.hotkey.bind({"rightalt", "shift"}, "9", moveToWorkspace(9))
hs.hotkey.bind({"rightalt", "shift"}, "0", moveToWorkspace(0))


function applicationWatcher(appName, eventType, appObject)
 if(eventType == hs.application.watcher.terminated) then
    onApplicationTerminated(appName, appObject)
  end
  if(eventType == hs.application.watcher.launched) then
    onApplicationLaunched(appName, appObject)
  end
    if (eventType == hs.application.watcher.activated) then
        if (appName == "Finder") then
            -- Bring all Finder windows forward when one gets activated
            appObject:selectMenuItem({"Window", "Bring All to Front"})
        end
    end
end
appWatcher = hs.application.watcher.new(applicationWatcher)
appWatcher:start()

-- Config Reload

function reloadConfig(files)
    doReload = false
    for _,file in pairs(files) do
        if file:sub(-4) == ".lua" then
            doReload = true
        end
    end
    if doReload then
        hs.reload()
    end
end

myWatcher = hs.pathwatcher.new(os.getenv("HOME") .. "/dotfiles/hammerspoon/.hammerspoon/", reloadConfig):start()
hs.alert.show("Config loaded")

-- Helpers
function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function len(o)
  local i = 0
  if type(o) == 'table' then
    for k,v in pairs(o) do
      i = i + 1
    end
  end
  return i
end

local clock = os.clock
function sleep(n)  -- seconds
  local t0 = clock()
  while clock() - t0 <= n do end
end

hs.timer.doEvery(0.1, function()

end)

sleep(1)
  print(dump())

for k,v in pairs(hs.window.allWindows()) do
  print(v:title())
end
