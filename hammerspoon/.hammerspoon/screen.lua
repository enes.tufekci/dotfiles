local screen = {}
GAP_OUTER = 16
GAP_INNER = GAP_OUTER / 2

function screen.alignLeft(win, ratio)
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  local totalWidth = (max.w / (ratio[1] + ratio[2]) * ratio[1])

  f.x = max.x + GAP_OUTER
  f.y = max.y + GAP_OUTER
  f.w = totalWidth - GAP_OUTER - GAP_INNER
  f.h = max.h - GAP_OUTER * 2
  win:setFrame(f)
end

function screen.alignRight(win, ratio)
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  local totalWidth = (max.w / (ratio[1] + ratio[2]) * ratio[2])
  local leftPadding = (max.w / (ratio[1] + ratio[2]) * ratio[1]) + GAP_INNER

  f.x = leftPadding
  f.y = max.y + GAP_OUTER
  f.w = totalWidth - GAP_OUTER - GAP_INNER
  f.h = max.h - GAP_OUTER * 2
  win:setFrame(f)
end

function screen.fullScreen(win)
  local f = win:frame()
  local screen = win:screen()
  local max = screen:frame()

  f.x = max.x + GAP_OUTER
  f.y = max.y + GAP_OUTER
  f.w = max.w - GAP_OUTER * 2
  f.h = max.h - GAP_OUTER * 2
  win:setFrame(f)
end

function screen.split(win1, win2, ratio)
  screen.alignLeft(win1, ratio)
  screen.alignRight(win2, ratio)
end

return screen
